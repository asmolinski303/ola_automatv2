from openpyxl import load_workbook
from PyPDF2 import PdfReader, PdfWriter
from tkinter import filedialog
from tkinter import *
from tkinter import ttk

import pathlib
import glob
import sys

from typing import List, Dict

'''
For now this should work like: python script.py [folder_name]
and encrypt all pdf in given folder with passwords given in procownicy.xlsx
in the working dir. Folder with pdfs should be in workign directory.

1. Will have to use argparse or maybe clic? - Not there. Tkinter

2. how to make exe from python? - nuitka > pyinstaller

3. do not take files if they are already password protected
'''
excel_name: str = None
dir_name: str = None

suffix = 'Kwitek_placowy'
password_col = "password"
email_col = "email"
file_col = "plik"

work_dir: str = None

def encrypt_pdfs():
    ph_files = get_pdfs(dir_name)
    files_pwds = make_file_password_dict()

    for file in ph_files:
        try:
            encrypt_pdf(f"{dir_name}/{file}", files_pwds[file])
        except KeyError as ke:
            print(f"failed: key error: {ke}")
        except Exception as e:
            print(f"failed: {e}")


def encrypt_pdf(input_file: str, password: str) -> None:
    file = PdfReader(input_file)
    writer = PdfWriter()

    for page in range(len(file.pages)):
        writer.add_page(file.pages[page])

    writer.encrypt(user_password=password, owner_password=None)

    with open(f"{input_file[:-4]}_encrypted.pdf", "wb") as fh:
        writer.write(fh)


def get_excel_sheet_content():
    sheet = load_workbook(filename=excel_name).active
    return [row for row in sheet.iter_rows(min_row=1, min_col=1, values_only=True)]


def get_pdfs(folder: str) -> List[str]:
    return [path.name for path in pathlib.Path(folder).glob(f'*{suffix}.pdf')] 


def make_file_password_dict():
    '''file name: NAME_SURNAME_Kwitek_placowy.pdf'''
    files = get_filenames()
    passwords = get_passwords()
    
    return dict(zip(files, passwords))


def get_passwords() -> List[str]:
    return get_column(password_col)


def get_filenames() ->List[str]:
    return [f"{item}_Kwitek_placowy.pdf" for item in get_fullnames()]


def get_fullnames():
    content = get_excel_sheet_content()
    i = 0
    name_col_index = -1
    fullname_col_index = -1
        
    for name in content[0]:
        if name == "Imię":
            name_col_index = i
        
        if name == "Nazwisko":
            fullname_col_index = i

        i += 1

    if name_col_index == -1 or fullname_col_index == -1:
        print('could not identify columns')
        return
    
    result = []
    for row in content[1:]:
        fullname = f"{row[name_col_index]}_{row[fullname_col_index]}"
        result.append(fullname)

    return result


def get_column(col_name: str):
    content = get_excel_sheet_content()
    i = 0
    num = -1
    
    for name in content[0]:
        if name == col_name:
            num = i
            break
        else:
            i += 1

    if num == -1:
        print("no such column")
        return
    else:
        result = []
        for row in content[1:]:
            result.append(row[i])

        return result



def get_headline():
    content = get_excel_sheet_content()

    result = []
    for row in content[0]:
        result.append(row)

    return result


def get_excel():
    return filedialog.askopenfilename()


def get_dir():
    return filedialog.askdirectory()


def run():
    root = Tk()
    root.title("Zahasłuj sobie PDFy")

    mainframe = ttk.Frame(root, padding="12 12 12 12")
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)


    def set_dir_var():
        global dir_name
        try:
            dir_var.set(get_dir())
            dir_name = dir_var.get()
            print(dir_name)
        except ValueError:
            pass
                
    dir_var = StringVar()
    dir_entry = ttk.Entry(mainframe, width=50, textvariable=dir_var)
    dir_entry.grid(column=1, row=1, sticky=(W, E))
    button_dir = ttk.Button(mainframe, text="katalog", command=set_dir_var)
    button_dir.grid(column=2, row=1, sticky=(W, E))


    def set_excel_var():
        global excel_name
        try:
            excel_var.set(get_excel())
            excel_name = excel_var.get()
            print(excel_name)
        except ValueError:
            pass

    excel_var = StringVar()
    excel_entry = ttk.Entry(mainframe, width=50, textvariable=excel_var)
    excel_entry.grid(column=1, row=2, sticky=(W, E))
    button_excel = ttk.Button(mainframe, text="excel", command=set_excel_var)
    button_excel.grid(column=2, row=2, sticky=(W, E))

    button_run = ttk.Button(mainframe, text="zahasłuj", command=encrypt_pdfs)
    button_run.grid(column=2, row=4, sticky=(W, E))

    root.mainloop()

if __name__ == "__main__":
    sys.exit(run())








